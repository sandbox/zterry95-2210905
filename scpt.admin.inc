<?php

/**
 *
 * @file
 * Administrative functionality for Empty Page module.
 *
 */
function scpt_admin_overview() {
  $header = array(
    t('Title'),
    t('Machine Name'),
    t('Path'),
    t('Created'),
    t('Status'),
    t('Operations')
  );
  foreach(scpt_load_multiple() as $name => $v) {
    $links = array(
      'edit' => array('title' => t('Edit'), 'href' => 'admin/structure/scpt/' . $v->machine_name . '/edit'),
      'delete' => array('title' => t('Delete'), 'href' => 'admin/structure/scpt/' . $v->machine_name . '/delete'),
    );
    $operation_links = theme('links', array('links' => $links));
    $rows[] = array(
      $v->title,
      $v->machine_name,
      l($v->path, $v->path, array('attributes' => array('target' => '_blank'))),
      $v->created,
      $v->status,
      $operation_links,
    );
  }
  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('Now pages yet..'),
  );
}
/**
 * Add, edit form
 */
function scpt_form($form, $form_state) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Title'),
  );

  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 21,
    '#machine_name' => array(
      'exists' => 'scpt_name_exists',
      'source' => array('title'),
      'label' => t('Machine Name'),
      'replace_pattern' => '[^a-z0-9-]+',
      'replace' => '_',
    ),
  );

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#maxlength' => 21,
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Status'),
	'#default_value' => TRUE,
  );

  $form['created'] = array(
    '#type' => 'hidden',
    '#value' => time(),
  );

  //proceed for default value..
  if (!empty($form_state['build_info']['args']) && $page = $form_state['build_info']['args'][0]) {
    foreach ($form as $k => $v) {
      $form[$k]['#default_value'] = $page->$k;
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

//proceed for form submit...
function scpt_form_submit($form, $form_state) {
  form_state_values_clean($form_state);
  scpt_insert($form_state['values']);
  drupal_set_message(t('@path created successfully!', array('@path' => $form_state['values']['path'])));
  menu_rebuild();
  drupal_goto('admin/structure/scpt');
}
/**
 * The Empty Page callback delete confirmation form.
 *
 * @param array $form_state
 * @param int $cid
 * @return array $form
 */
function scpt_delete_form($form, &$form_state, $page) {
  // Get current editor properties.
  drupal_set_title('Delete callback');

  $form = confirm_form(
    array(
      'name' => array(
        '#type' => 'value',
        '#value' => $page->machine_name,
      ),
    ),
    t('Are you sure you want to delete the page for <em>!path</em>?', array('!path' => $page->machine_name)),
    'admin/structure/scpt',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );

  return $form;
}
function scpt_delete_form_submit($form, $form_state) {
  scpt_delete($form_state['values']['name']);
  drupal_set_message(t('@name delete successfully!', array('@name' => $form_state['values']['name'])));
  menu_rebuild();
  drupal_goto('admin/structure/scpt');
}
/**
 * Hepler function to detect if a machine name existing or not.
 */
function scpt_name_exists($name) {
  return scpt_load($name) ? TRUE : FALSE;
}

